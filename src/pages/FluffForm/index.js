import Question from "../../containers/Question";
import { questions } from "./question";
import { FiArrowLeft, FiArrowRight } from "react-icons/fi";
import "./style.css";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
const FluffForm = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [error, setError] = useState(false);
  const validate = () => {
    if (answers.length < 4) {
      setError(true);
      return false;
    }
    return true;
  };
  const answers = useSelector((state) =>
    state.questions.filter(
      (item) =>
        item.id > (currentPage - 1) * 4 && item.id <= (currentPage - 1) * 4 + 4
    )
  );
  useEffect(() => {
    console.log(answers);
  }, [answers]);
  const onNavigateClick = (direction) => {
    setError(false);
    if (
      (currentPage === 1 && direction === "back") ||
      (currentPage === 5 && direction === "front")
    ) {
      return;
    }
    if (direction === "front" && !validate()) {
      return false;
    }
    if (direction === "back") {
      setCurrentPage(currentPage - 1);
    } else {
      setCurrentPage(currentPage + 1);
    }
  };
  return (
    <div className="form-container container">
      <div className="form-main-content">
        <h1>Fluff Form</h1>
        <div className="form-progress">
          <span className="form-status">{currentPage}/5</span>
        </div>
      </div>
      {questions
        .slice((currentPage - 1) * 4, (currentPage - 1) * 4 + 4)
        .map(({ question, id, options }) => (
          <Question
            key={id}
            question={question}
            id={id}
            options={options}
            error={error}
          />
        ))}
      <div className="navigator">
        <FiArrowLeft
          onClick={() => onNavigateClick("back")}
          className={`icon-button  ${currentPage === 1 ? "icon-disabled" : ""}`}
        />
        <FiArrowRight
          onClick={() => onNavigateClick("front")}
          className={`icon-button  ${currentPage === 5 ? "icon-disabled" : ""}`}
        />
      </div>
    </div>
  );
};

export default FluffForm;
