export const questions = [
    {
      id: 1,
      question: "Do these strike you?",
      options: [
        {
          id: 1,
          option: "I’m alone",
        },
        {
          id: 2,
          option: "Need someone to listen",
        },
        {
          id: 3,
          option: "Stressed with work",
        },
        {
          id: 4,
          option: "I'm worried for no reason",
        },
      ],
    },
    {
      id: 2,
      question: "When you need a break:",
      options: [
        {
          id: 1,
          option: "I want to be left alone.",
        },
        {
          id: 2,
          option: "I want to go out",
        },
        {
          id: 3,
          option: "Binge watch",
        },
        {
          id: 4,
          option: "Everything",
        },
      ],
    },
    {
      id: 3,
      question: "Thought on your work/studies",
      options: [
        {
          id: 1,
          option: "I’m Done",
        },
        {
          id: 2,
          option: "Going fine",
        },
        {
          id: 3,
          option: "No life",
        },
        {
          id: 4,
          option: "No comments",
        },
      ],
    },
    {
      id: 4,
      question: "I be like",
      options: [
        {
          id: 1,
          option: "literally bounce off the walls",
        },
        {
          id: 2,
          option: "talk till dawn",
        },
        {
          id: 3,
          option: "like to be home",
        },
        {
          id: 4,
          option: "love seeking peace",
        },
      ],
    },
    {
        id: 5,
        question: "How tuff you are?",
        options: [
          {
            id: 1,
            option: "Literally make friends with everyone",
          },
          {
            id: 2,
            option: "I have a circle",
          },
          {
            id: 3,
            option: "Easy",
          },
          {
            id: 4,
            option: "Not easy",
          },
        ],
      },
      {
        id: 6,
        question: "What game you'll chose?",
        options: [
          {
            id: 1,
            option: "Board",
          },
          {
            id: 2,
            option: "Computer",
          },
          {
            id: 3,
            option: "Hide and seek",
          },
          {
            id: 4,
            option: "I rather scroll",
          },
        ],
      },
      {
        id: 7,
        question: "For future,",
        options: [
          {
            id: 1,
            option: "I have long plans",
          },
          {
            id: 2,
            option: "I know what I do tomorrow",
          },
          {
            id: 3,
            option: "I’m in a rollercoster",
          },
          {
            id: 4,
            option: "Complicated",
          },
        ],
      },
      {
        id: 8,
        question: "You won a lottery, you'll",
        options: [
          {
            id: 1,
            option: "Buy lux wheels",
          },
          {
            id: 2,
            option: "Travel around",
          },
          {
            id: 3,
            option: "Buy a home",
          },
          {
            id: 4,
            option: "Stay low",
          },
        ],
      },
      {
        id: 9,
        question: "You have to depart tomorrow 10 AM",
        options: [
          {
            id: 1,
            option: "I will pack it today night",
          },
          {
            id: 2,
            option: "That's a lot of time tomorrow",
          },
          {
            id: 3,
            option: "I'm ready",
          },
          {
            id: 4,
            option: "Hey Mom..",
          },
        ],
      },
      {
        id: 10,
        question: "You failed this time",
        options: [
          {
            id: 1,
            option: "let me regret sometime, and let go",
          },
          {
            id: 2,
            option: "Ah sh*t, Here we go again",
          },
          {
            id: 3,
            option: "It gonna haunt me",
          },
          {
            id: 4,
            option: "Will Smith(motivation)",
          },
        ],
      },
      {
        id: 11,
        question: "Pick something",
        options: [
          {
            id: 1,
            option: "Books",
          },
          {
            id: 2,
            option: "Movies",
          },
          {
            id: 3,
            option: "Food",
          },
          {
            id: 4,
            option: "Ride",
          },
        ],
      },
      {
        id: 12,
        question: "People call me",
        options: [
          {
            id: 1,
            option: "Fox - sneaky",
          },
          {
            id: 2,
            option: "Black horse - strategic",
          },
          {
            id: 3,
            option: "Lion - My own territory",
          },
          {
            id: 4,
            option: "Cat - I'm complicated",
          },
        ],
      },
      {
        id: 13,
        question: "A thorn just pierced in",
        options: [
          {
            id: 1,
            option: "I’m gonna die",
          },
          {
            id: 2,
            option: "I don't care",
          },
          {
            id: 3,
            option: "Greys Anatomy",
          },
          {
            id: 4,
            option: "Ah sh*t!",
          },
        ],
      },
      {
        id: 14,
        question: "Okay, you be the leader?",
        options: [
          {
            id: 1,
            option: "I’m the one",
          },
          {
            id: 2,
            option: "I'll rather die",
          },
          {
            id: 3,
            option: "I can't handle myself",
          },
          {
            id: 4,
            option: "whatever",
          },
        ],
      },
      {
        id: 15,
        question: "In a arguement",
        options: [
          {
            id: 1,
            option: "I rather don't put my back in",
          },
          {
            id: 2,
            option: "Curius to know",
          },
          {
            id: 3,
            option: "Rather solve",
          },
          {
            id: 4,
            option: "Don't care + ratio",
          },
        ],
      },
      {
        id: 16,
        question: "Mood swings",
        options: [
          {
            id: 1,
            option: "That day is gone",
          },
          {
            id: 2,
            option: "Can handle",
          },
          {
            id: 3,
            option: "intolerable but will tolerate",
          },
          {
            id: 4,
            option: "I'll cry",
          },
        ],
      },
      {
        id: 17,
        question: "Plan A failed",
        options: [
          {
            id: 1,
            option: "I must be having Plan B",
          },
          {
            id: 2,
            option: "Let's see", 
          },
          {
            id: 3,
            option: "I built alpha",
          },
          {
            id: 4,
            option: "Quit",
          },
        ],
      },
      {
        id: 18,
        question: "Choose one",
        options: [
          {
            id: 1,
            option: "Beach",
          },
          {
            id: 2,
            option: "Museums",
          },
          {
            id: 3,
            option: "Hectic trekk",
          },
          {
            id: 4,
            option: "Peace",
          },
        ],
      },
      {
        id: 19,
        question: "Past hurts",
        options: [
          {
            id: 1,
            option: "No way!",
          },
          {
            id: 2,
            option: "You telling truth",
          },
          {
            id: 3,
            option: "Sometimes",
          },
          {
            id: 4,
            option: "Get over it",
          },
        ],
      },
      {
        id: 20,
        question: "Anger management",
        options: [
          {
            id: 1,
            option: "I'm hulk sometimes",
          },
          {
            id: 2,
            option: "Good at it",
          },
          {
            id: 3,
            option: "Depends",
          },
          {
            id: 4,
            option: "Barely feel angry",
          },
        ],
      },
  ];