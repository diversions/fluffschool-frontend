import DogAnimation from "../../assets/lotties/home-dog.json";
import { FiArrowRightCircle } from "react-icons/fi";
import "./style.css";
import LottieAnimation from "../../containers/LottieAnimation";
import CatAnimation from "../../assets/lotties/46472-lurking-cat.json";
import YoungLady from "../../assets/lotties/YoungLady.json";
import { useNavigate } from "react-router";
const Landing = () => {
  const navigate = useNavigate();
  const goToForm = () => {
    navigate("/formfluff");
  };
  return (
    <div className="container">
      <div className="main-content">
        <h3>Unconditional Love isn’t a myth</h3>
        <div className="extra-large-content">
          {/* A House is never lonely where a loving dog waits. */}A Stressful
          Job Needs a Fluffy Therapy.
        </div>
      </div>
      <div className="secondary-content flex">
        <div className="lottie-dog flex">
          <LottieAnimation animationData={DogAnimation} />
        </div>
        <div className="landing-action-content flex">
          <h2>Find your partner*</h2>
          <h3>
            With a 20 questions, we will help you find who can be your
            fluffball.
          </h3>
          <span className="icon-holder">
            <FiArrowRightCircle className="icon-button" onClick={goToForm} />
          </span>
        </div>
      </div>
      <div className="secondary-content second-item flex">
        <div className="landing-action-content flex">
          <h2>We are Curious.</h2>
          <h3>Studies show that pets can be great stress busters</h3>
        </div>

        <div className="lottie-cat flex">
          <LottieAnimation
            height={200}
            width={200}
            animationData={CatAnimation}
          />
        </div>
      </div>
      <div className="secondary-content third-item flex">
        <div className="lottie-lady flex">
          <LottieAnimation height={200} width={200} animationData={YoungLady} />
        </div>
        <div className="landing-action-content flex">
          <h2>We are Professionals.</h2>
          <h3>6 Out of 10 US homes employs a Fluff.</h3>
        </div>
      </div>
    </div>
  );
};

export default Landing;
