import { Link } from "react-router-dom";
import Logo from "../../assets/logos/logo.png";
import LogoInvert from "../../assets/logos/logoInvert.png";
import "./style.css";
const Navbar = () => {
  return (
    <div className="navbar">
      <div className="navbar-container">
        <div className="main-title-container flex">
          <img height="60px" src={LogoInvert} alt="paw in the house" />
          <div className="main-title"><Link to="/">Fluffschool</Link></div>
        </div>
        <div className="menu-items flex">
          <ul>
            <li>
              <strong><Link to="/formfluff">Find Fluff</Link></strong>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
