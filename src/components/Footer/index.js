import "./style.css";
const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <p className="copyright">© 2021 Fluffschool</p>
      </div>
    </div>
  );
};

export default Footer;
