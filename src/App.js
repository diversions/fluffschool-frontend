import { createStore } from "redux";
import "./App.css";
import AppRouter from "./AppRouter";
import PageWithHeaderAndFooter from "./containers/PageWithHeaderAndFooter";
function App() {
  return (
    <div className="app">
      <AppRouter/>
    </div>
  );
}

export default App;
