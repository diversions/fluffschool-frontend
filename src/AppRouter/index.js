import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Footer from "../components/Footer";
import Navbar from "../components/Navbar";
import FluffForm from "../pages/FluffForm";
import Landing from "../pages/Landing";

const AppRouter = () => {
  return (
    <Router>
    <Navbar />
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/formfluff" element={<FluffForm />} />
      </Routes>
      <Footer />
    </Router>
  );
};

export default AppRouter;
