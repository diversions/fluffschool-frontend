export const questionReducer = (state = [], action) => {
  switch (action.type) {
    case "ADDORUPDATE":
      let questionIndex = state.findIndex(
        (item) => item.id === action.payload.id
      );
      if (questionIndex > -1) {
        state[questionIndex] = { ...action.payload };
      } else {
        state.push(action.payload);
      }
      return [...state];
    case "CLEARSELECTION":
      state = state.filter((item) => item.id !== action.payload.id);
      return [...state];
    default:
      return [...state];
  }
};
