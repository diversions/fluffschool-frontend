import { Route } from "react-router-dom";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
const PageWithHeaderAndFooter = ({ component }) => {
  return (
    <>
      <Navbar />
      {component}
      <Footer />
    </>
  );
};
export default PageWithHeaderAndFooter;
