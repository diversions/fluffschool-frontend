import { useDispatch, useSelector } from "react-redux";
import { addorUpdate } from "../../actions/questionActions";
import "./style.css";
const Question = ({ id, question, options, error }) => {
  const dispatch = useDispatch();
  const answerObj = useSelector((state) =>
    state.questions.find((item) => item.id === id)
  );
  const onAnswerSelect = (answer) => {
    dispatch(addorUpdate({ id, question, options, answer }));
  };
  return (
    <div key={id} className="question-container">
      <div className={`question ${(error && !answerObj) && 'question-error'}`}>{question}</div>
      <div className="option-container">
        {options.map(({ option, id: optionID }) => (
          <div
            key={optionID}
            onClick={() => onAnswerSelect(optionID)}
            className={`option-default option flex ${
              answerObj?.answer === optionID ? "answer" : "option"
            }`}
          >
            {option}
          </div>
        ))}
      </div>
    </div>
  );
};

export default Question;
