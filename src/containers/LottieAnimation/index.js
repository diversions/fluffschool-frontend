import Lottie from "react-lottie";

const LottieAnimation = ({ animationData, height, width, ...options }) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
    ...options,
  };
  return (
    <>
      <Lottie height={height} width={width} options={defaultOptions}></Lottie>
    </>
  );
};

export default LottieAnimation;
